package gt.megapaca.mproject.rest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.annotation.Bean
import org.springframework.web.client.RestTemplate

@SpringBootApplication
class App {

    @Bean
    fun getRestTemplate(): RestTemplate? {
        return RestTemplate()
    }

}

fun main(args: Array<String>) {
    SpringApplicationBuilder(App::class.java)
        .properties("spring.config.name:mega-ws")
        .build()
        .run(*args)
}
