package gt.megapaca.mproject.rest

import gt.megapaca.mproject.infrastructure.ConfigApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@EnableAutoConfiguration
@ComponentScan(value = ["gt.megapaca"])
@Import(value = [ConfigApplication::class])
class AppConfig {
}