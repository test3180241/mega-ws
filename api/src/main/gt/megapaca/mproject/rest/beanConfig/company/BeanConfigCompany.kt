package gt.megapaca.mproject.rest.beanConfig.company

import gt.megapaca.mproject.application.company.create.CompanyCreator
import gt.megapaca.mproject.application.company.find.CompanyFinder
import gt.megapaca.mproject.application.company.update.CompanyUpdater
import gt.megapaca.mproject.domain.repository.CompanyRepository
import gt.megapaca.mproject.domain.repository.CountryRepository
import gt.megapaca.mproject.infrastructure.config.ConfigRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(value = [ConfigRepository::class])
class BeanConfigCompany {

    @Bean
    fun companyCreator(
        companyRepository: CompanyRepository,
        countryRepository: CountryRepository
    ): CompanyCreator {
        return CompanyCreator(companyRepository, countryRepository)
    }

    @Bean
    fun companyUpdater(
        companyRepository: CompanyRepository,
        countryRepository: CountryRepository
    ): CompanyUpdater {
        return CompanyUpdater(companyRepository, countryRepository)
    }

    @Bean
    fun companyFinder(
        companyRepository: CompanyRepository
    ): CompanyFinder {
        return CompanyFinder(companyRepository)
    }
}