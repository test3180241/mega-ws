package gt.megapaca.mproject.rest.beanConfig.country

import gt.megapaca.mproject.application.country.find.CountryByIdFinder
import gt.megapaca.mproject.application.country.find.CountryFinder
import gt.megapaca.mproject.domain.repository.CountryRepository
import gt.megapaca.mproject.infrastructure.config.ConfigRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(value = [ConfigRepository::class])
class BeanConfigCountry {

    @Bean
    fun countryFinder(
        countryRepository: CountryRepository
    ): CountryFinder {
        return CountryFinder(countryRepository)
    }

    @Bean
    fun countryByIdFinder(
        countryRepository: CountryRepository
    ): CountryByIdFinder {
        return CountryByIdFinder(countryRepository)
    }
}