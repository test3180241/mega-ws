package gt.megapaca.mproject.rest.controller.company

import gt.megapaca.mproject.application.company.find.CompanyResponse
import gt.megapaca.mproject.rest.controller.country.CountryJSONDTO

class CompanyJSONDTO {

    var id: Long = 0
    var active: Boolean = true
    var name: String = ""
    var countryId: Long = 0
    var taxCode: String = ""
    var country: CountryJSONDTO = CountryJSONDTO()

    constructor(){
    }

    constructor(company: CompanyResponse) {
        id = company.id
        active = company.active
        name = company.name
        countryId = company.country
        taxCode = company.taxCode
    }

}