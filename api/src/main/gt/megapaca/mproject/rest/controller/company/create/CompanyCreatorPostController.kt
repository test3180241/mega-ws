package gt.megapaca.mproject.rest.controller.company.create

import gt.megapaca.mproject.application.company.CompanyRequest
import gt.megapaca.mproject.application.company.create.CompanyCreator
import gt.megapaca.mproject.rest.controller.company.CompanyJSONDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse

@Api(value = "/api/Company", description = "API to create a Company")
@RestController
@RequestMapping(
    value = ["/api/Company"],
    produces = [MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE]
)
class CompanyCreatorPostController {

    @Autowired
    private lateinit var companyCreator: CompanyCreator

    @ApiOperation(
        value = "Create company",
        notes = "Create new company and return companyId"
    )
    @ApiResponse(code = HttpServletResponse.SC_CREATED, message = "Company ID")
    @Transactional(rollbackFor = [Exception::class])
    @PostMapping(value = ["/", ""])
    fun index(
        @RequestBody company: CompanyJSONDTO
    ): ResponseEntity<Long> {

        val companyRequest = CompanyRequest.build {
            this.name = company.name
            this.country = company.countryId
            this.taxCode = company.taxCode
        }

        val companyId = companyCreator.create(companyRequest)


        return ResponseEntity
            .status(HttpStatus.CREATED)
            .body(companyId)
    }
}