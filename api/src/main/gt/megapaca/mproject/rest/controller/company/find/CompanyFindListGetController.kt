package gt.megapaca.mproject.rest.controller.company.find

import gt.megapaca.mproject.application.company.find.CompanyFinder
import gt.megapaca.mproject.application.country.find.CountryByIdFinder
import gt.megapaca.mproject.rest.controller.company.CompanyJSONDTO
import gt.megapaca.mproject.rest.controller.country.CountryJSONDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse

@Api(value = "/api/Company", description = "API to get a company list")
@RestController
@RequestMapping(
    value = ["/api/Company"],
    produces = [MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE]
)
class CompanyFindListGetController {

    @Autowired
    private lateinit var companyFinder: CompanyFinder

    @Autowired
    private lateinit var countryByIdFinder: CountryByIdFinder
    @ApiOperation(
        value = "Find company",
        notes = "Find an company list"
    )
    @ApiResponse(code = HttpServletResponse.SC_OK, message = "Company list")
    @Transactional(readOnly = true)
    @GetMapping(value = ["", "/"])
    fun index(): ResponseEntity<List<CompanyJSONDTO>> {

        val result = companyFinder
            .findAll()
            .map { CompanyJSONDTO(it) }

        result.forEach{it.country = CountryJSONDTO(countryByIdFinder.find(it.countryId).get()) }

        return ResponseEntity
            .status(HttpStatus.OK)
            .body(result)
    }
}