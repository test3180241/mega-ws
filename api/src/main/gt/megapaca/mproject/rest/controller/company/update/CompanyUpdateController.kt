package gt.megapaca.mproject.rest.controller.company.update

import gt.megapaca.mproject.application.company.CompanyRequest
import gt.megapaca.mproject.application.company.update.CompanyUpdater
import gt.megapaca.mproject.rest.controller.company.CompanyJSONDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse

@Api(value = "/api/Company", description = "API to update a company")
@RestController
@RequestMapping(
    value = ["/api/Company"],
    produces = [MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE]
)
class CompanyUpdateController {

    @Autowired
    private lateinit var companyUpdater: CompanyUpdater

    @ApiOperation(
        value = "Update company",
        notes = "Update company and return companyId"
    )
    @ApiResponse(code = HttpServletResponse.SC_OK, message = "Company ID")
    @Transactional(rollbackFor = [Exception::class])
    @PutMapping(value = ["/{id:.*}"])
    fun index(
        @PathVariable("id") id: Long,
        @RequestBody company: CompanyJSONDTO
    ): ResponseEntity<Long> {

        val companyRequest = CompanyRequest.build {
            this.id = company.id
            this.active = company.active
            this.name = company.name
            this.country = company.countryId
            this.taxCode = company.taxCode
        }

        val companyId = companyUpdater.update(companyRequest)

        return ResponseEntity
            .status(HttpStatus.OK)
            .body(companyId)
    }
}