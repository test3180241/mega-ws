package gt.megapaca.mproject.rest.controller.country

import gt.megapaca.mproject.application.country.find.CountryResponse

class CountryJSONDTO {

    var id: Long = 0
    var active: Boolean = true
    var name: String = ""
    var taxCodeFormat: String = ""

    constructor(){

    }

    constructor(country: CountryResponse) {
        id = country.id
        active = country.active
        name = country.name
        taxCodeFormat = country.taxCodeFormat
    }
}