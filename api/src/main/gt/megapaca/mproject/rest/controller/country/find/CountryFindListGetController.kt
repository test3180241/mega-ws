package gt.megapaca.mproject.rest.controller.country.find

import gt.megapaca.mproject.application.country.find.CountryFinder
import gt.megapaca.mproject.rest.controller.country.CountryJSONDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse

@Api(value = "/api/Country", description = "API to get a country list")
@RestController
@RequestMapping(
    value = ["/api/Country"],
    produces = [MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE]
)
class CountryFindListGetController {

    @Autowired
    private lateinit var countryFinder: CountryFinder

    @ApiOperation(
        value = "Find country",
        notes = "Find an country list"
    )
    @ApiResponse(code = HttpServletResponse.SC_OK, message = "Country list")
    @Transactional(readOnly = true)
    @GetMapping(value = ["", "/"])
    fun index(): ResponseEntity<List<CountryJSONDTO>> {

        val result = countryFinder
            .findAll()
            .map { CountryJSONDTO(it) }

        return ResponseEntity
            .status(HttpStatus.OK)
            .body(result)
    }
}