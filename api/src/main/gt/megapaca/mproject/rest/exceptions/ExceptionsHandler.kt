package gt.megapaca.mproject.rest.exceptions

import gt.megapaca.mproject.application.exceptions.ConflictDataException
import gt.megapaca.mproject.application.exceptions.EntityAlreadyExistsException
import gt.megapaca.mproject.application.exceptions.EntityNotFoundException
import gt.megapaca.mproject.application.exceptions.InvalidTaxCodeException
import gt.megapaca.mproject.domain.exceptions.InvalidDataException
import gt.megapaca.mproject.domain.exceptions.RequiredValueException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody


@ControllerAdvice
class ExceptionsHandler {

    @ResponseBody
    @ExceptionHandler(EntityNotFoundException::class)
    fun entityNotFoundExceptionHandler(ex: EntityNotFoundException): ResponseEntity<Map<String, String>> {
        val response = mutableMapOf<String, String>()
        response["title"] = "EntityNotFoundException"
        response["message"] = ex.message ?: "Entity not found"

        return ResponseEntity
            .status(HttpStatus.NOT_FOUND)
            .body(response)
    }

    @ResponseBody
    @ExceptionHandler(RequiredValueException::class)
    fun requiredValueExceptionHandler(ex: RequiredValueException): ResponseEntity<Map<String, String>> {
        val response = mutableMapOf<String, String>()
        response["title"] = "RequiredValueException"
        response["message"] = ex.message ?: "Required value"

        return ResponseEntity
            .status(HttpStatus.NOT_ACCEPTABLE)
            .body(response)
    }

    @ResponseBody
    @ExceptionHandler(EntityAlreadyExistsException::class)
    fun entityAlreadyExistsExceptionHandler(ex: EntityAlreadyExistsException): ResponseEntity<Map<String, String>> {
        val response = mutableMapOf<String, String>()
        response["title"] = "EntityAlreadyExistsException"
        response["message"] = ex.message ?: "Entity already exist"

        return ResponseEntity
            .status(HttpStatus.CONFLICT)
            .body(response)
    }

    @ResponseBody
    @ExceptionHandler(InvalidDataException::class)
    fun entityAlreadyExistsExceptionHandler(ex: InvalidDataException): ResponseEntity<Map<String, String>> {
        val response = mutableMapOf<String, String>()
        response["title"] = "InvalidDataException"
        response["message"] = ex.message ?: "Invalid data exception"

        return ResponseEntity
            .status(HttpStatus.NOT_ACCEPTABLE)
            .body(response)
    }

    @ResponseBody
    @ExceptionHandler(InvalidTaxCodeException::class)
    fun conflictDataExceptionHandler(ex: InvalidTaxCodeException): ResponseEntity<Map<String, String>> {
        val response = mutableMapOf<String, String>()
        response["title"] = "invalidTaxCodeException"
        response["message"] = ex.message ?: "Incorrect format"

        return ResponseEntity
            .status(HttpStatus.CONFLICT)
            .body(response)
    }

}
