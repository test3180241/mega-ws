package gt.megapaca.mproject.application.company

class CompanyRequest {

    companion object {
        fun build(init: CompanyRequest.() -> Unit) = CompanyRequest().apply(init)
    }

    var id = 0L
    var active: Boolean = true
    var name: String = ""
    var country : Long = 0
    var taxCode: String = ""
}