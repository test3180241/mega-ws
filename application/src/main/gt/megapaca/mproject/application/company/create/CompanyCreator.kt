package gt.megapaca.mproject.application.company.create

import gt.megapaca.mproject.application.company.CompanyRequest
import gt.megapaca.mproject.application.exceptions.EntityNotFoundException
import gt.megapaca.mproject.domain.entities.company.Company
import gt.megapaca.mproject.domain.entities.company.CompanyCountry
import gt.megapaca.mproject.domain.entities.company.CompanyName
import gt.megapaca.mproject.domain.entities.company.CompanyTaxCode
import gt.megapaca.mproject.application.exceptions.InvalidTaxCodeException
import gt.megapaca.mproject.domain.repository.CompanyRepository
import gt.megapaca.mproject.domain.repository.CountryRepository

class CompanyCreator(
    private val companyRepository: CompanyRepository,
    private val countryRepository: CountryRepository
) {

    fun create(request: CompanyRequest): Long {

        // 1. Validate if countryRepository
        val country = countryRepository.findById(request.country)
        if (country.isEmpty) {
            throw EntityNotFoundException("Country does not exist")
        }

        //2.Validate the tax code format

        val taxCodeFormatRegex = Regex(country.get().taxCodeFormat.value)
        if (!taxCodeFormatRegex.matches(request.taxCode)) {
            throw InvalidTaxCodeException("Tax code format is invalid")
        }

        // 2. Create company entity
        val company = Company(
            CompanyName(request.name),
            CompanyCountry(request.country),
            CompanyTaxCode(request.taxCode)
        )

        return companyRepository.create(company)
    }

}