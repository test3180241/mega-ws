package gt.megapaca.mproject.application.company.find

import gt.megapaca.mproject.domain.repository.CompanyRepository

class CompanyFinder(
    private val companyRepository: CompanyRepository
) {

    fun findAll(): List<CompanyResponse> {
        return companyRepository.findAll()
            .map(::CompanyResponse)
    }


}