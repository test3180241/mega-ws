package gt.megapaca.mproject.application.company.find

import gt.megapaca.mproject.domain.entities.company.Company

data class CompanyResponse(
    val id: Long,
    val active: Boolean,
    val name: String,
    val country: Long = 0,
    val taxCode: String
) {

    constructor(company: Company): this(
        company.id,
        company.active,
        company.name.value,
        company.country.value,
        company.taxCode.value
    ) {
        // Create from domain class
    }


}