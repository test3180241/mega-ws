package gt.megapaca.mproject.application.company.update

import gt.megapaca.mproject.application.company.CompanyRequest
import gt.megapaca.mproject.application.exceptions.EntityNotFoundException
import gt.megapaca.mproject.domain.entities.company.CompanyCountry
import gt.megapaca.mproject.domain.entities.company.CompanyName
import gt.megapaca.mproject.domain.entities.company.CompanyTaxCode
import gt.megapaca.mproject.application.exceptions.InvalidTaxCodeException
import gt.megapaca.mproject.domain.repository.CompanyRepository
import gt.megapaca.mproject.domain.repository.CountryRepository

class CompanyUpdater(
    private val companyRepository: CompanyRepository,
    private val countryRepository: CountryRepository
) {

    fun update(request: CompanyRequest): Long {
        // 1. Find company
        val company = companyRepository
            .findById(request.id)
            .orElseThrow { EntityNotFoundException("Company not found $request.id") }

        // 2. Validate if countryRepository
        val country = countryRepository.findById(request.country)
        if (country.isEmpty) {
            throw EntityNotFoundException("Country does not exist")
        }

        //3.Validate the tax code format

        val taxCodeFormatRegex = Regex(country.get().taxCodeFormat.value)
        if (!taxCodeFormatRegex.matches(request.taxCode)) {
            throw InvalidTaxCodeException("Tax code format is invalid")
        }

        // 4. Update company entity
        company.name = CompanyName(request.name)
        company.country = CompanyCountry(request.country)
        company.taxCode = CompanyTaxCode(request.taxCode)


        return companyRepository.update(company)
    }
}