package gt.megapaca.mproject.application.country


class CountryRequest {

    companion object {
        fun build(init: CountryRequest.() -> Unit) = CountryRequest().apply(init)
    }

    var id = 0L
    var active: Boolean = true
    var name: String = ""
    var taxCodeFormat: String = ""
}