package gt.megapaca.mproject.application.country.find

import gt.megapaca.mproject.domain.repository.CountryRepository
import java.util.*

class CountryByIdFinder(
    private val countryRepository: CountryRepository
) {

    fun find(id: Long): Optional<CountryResponse> {
        val country = countryRepository.findById(id)

        return if (country.isPresent) {
            Optional.of(CountryResponse(country.get()))
        } else {
            Optional.empty()
        }
    }

}