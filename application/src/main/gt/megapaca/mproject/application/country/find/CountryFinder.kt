package gt.megapaca.mproject.application.country.find

import gt.megapaca.mproject.domain.repository.CountryRepository

class CountryFinder(
    private val countryRepository: CountryRepository
) {

    fun findAll(): List<CountryResponse> {
        return countryRepository.findAll()
            .map(::CountryResponse)
    }

}