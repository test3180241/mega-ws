package gt.megapaca.mproject.application.country.find

import gt.megapaca.mproject.domain.entities.country.Country


data class CountryResponse(
    val id: Long,
    val active: Boolean,
    val name: String,
    val taxCodeFormat: String
) {

    constructor(country: Country): this(
        country.id,
        country.active,
        country.name.value,
        country.taxCodeFormat.value
    ) {
        // Create from domain class
    }

}