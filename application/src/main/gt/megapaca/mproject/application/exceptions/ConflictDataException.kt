package gt.megapaca.mproject.application.exceptions

class ConflictDataException(errorMessage: String) : Exception(errorMessage)
