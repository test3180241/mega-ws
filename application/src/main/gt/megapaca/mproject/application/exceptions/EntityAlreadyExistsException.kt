package gt.megapaca.mproject.application.exceptions

class EntityAlreadyExistsException(errorMessage: String) : Exception(errorMessage)
