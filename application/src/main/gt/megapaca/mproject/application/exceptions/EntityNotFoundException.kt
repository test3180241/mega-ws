package gt.megapaca.mproject.application.exceptions

class EntityNotFoundException(errorMessage: String) : Exception("Entity not found $errorMessage")
