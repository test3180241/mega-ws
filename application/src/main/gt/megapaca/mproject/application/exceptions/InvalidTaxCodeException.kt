package gt.megapaca.mproject.application.exceptions

class InvalidTaxCodeException(message: String) : Exception(message) {
}