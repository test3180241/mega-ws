package gt.megapaca.mproject.application.exceptions

class PasswordValidationException(errorMessage: String) : Exception(errorMessage)
