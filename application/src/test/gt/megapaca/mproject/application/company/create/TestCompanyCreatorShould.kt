package gt.megapaca.mproject.application.company.create

import gt.megapaca.mproject.application.company.shared.CompanyModuleUnitTestCase
import gt.megapaca.mproject.application.company.shared.CompanyMother
import gt.megapaca.mproject.application.country.shared.CountryMother
import gt.megapaca.mproject.application.exceptions.EntityNotFoundException
import gt.megapaca.mproject.application.exceptions.InvalidTaxCodeException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import io.mockk.every
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestCompanyCreatorShould : CompanyModuleUnitTestCase() {

    @Test
    fun `throw exception when country does not exists`() {
        val request = CompanyMother.randomRequest()

        //Primera validacion del caso de uso
        every {
            countryRepository.findById(allAny())
        } returns Optional.empty()

        //Si no encuentra pais entra la excepcion
        assertThrows(
            EntityNotFoundException::class.java,
            { companyCreator.create(request) },
            "throw exception because country does not exists"
        )

    }

    @Test
    fun `throw exception when country tax code format does not match`() {
        val request = CompanyMother.randomRequest()
        val country = CountryMother.random()
        //Esta es una expresion regular que solo contiene ABC
        country.taxCodeFormat.value = "[ABC]"

        //Pára que la excepcion se de
        request.taxCode = "123"

        //Aqui paso a la segunda validacion, pero ya le mando un pais para que
        //se la salte y entre a la siguiente validacion
        every {
            countryRepository.findById(allAny())
        } returns Optional.of(country)

        assertThrows(
            InvalidTaxCodeException::class.java,
            { companyCreator.create(request) },
            "throw exception because country does not match"
        )


    }

    @Test
    fun `create a company`() {
        val request = CompanyMother.randomRequest()

        val country = CountryMother.random()

        //Esta es una expresion regular que solo contiene ABC
        country.taxCodeFormat.value = "[1-4]*"

        //Pára que la coincida la expresion
        request.taxCode = "12"

        //Aqui paso a la segunda validacion, pero ya le mando un pais para que
        //se la salte y entre a la siguiente validacion
        every {
            countryRepository.findById(allAny())
        } returns Optional.of(country)

        every {
            companyRepository.create(allAny())
        } returns request.id

        assertEquals(
            companyCreator.create(request),
            request.id,
            "Country ID must be equals"
        )
    }
}