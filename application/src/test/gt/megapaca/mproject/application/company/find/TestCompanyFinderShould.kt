package gt.megapaca.mproject.application.company.find

import gt.megapaca.mproject.application.company.shared.CompanyModuleUnitTestCase
import gt.megapaca.mproject.application.company.shared.CompanyMother
import io.mockk.every
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.stream.Collectors

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestCompanyFinderShould : CompanyModuleUnitTestCase() {

    @Test
    fun `return empty list when company not found`() {
        every {
            companyRepository.findAll()
        } returns emptyList()

        val result = companyFinder.findAll()

        assertTrue(result.isEmpty(), "List must be empty")
    }

    @Test
    fun `return company list data`() {
        val listBase = CompanyMother.randomList(3)

        val responseListBase = listBase
            .stream()
            .map { CompanyResponse(it) }
            .collect(Collectors.toList())

        every {
            companyRepository.findAll()
        } returns listBase

        val result = companyFinder.findAll()

        assertEquals(responseListBase, result, "Company lists must be equals")
    }
}