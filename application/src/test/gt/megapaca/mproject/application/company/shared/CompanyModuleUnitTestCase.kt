package gt.megapaca.mproject.application.company.shared

import gt.megapaca.mproject.application.company.create.CompanyCreator
import gt.megapaca.mproject.application.company.find.CompanyFinder
import gt.megapaca.mproject.application.company.update.CompanyUpdater
import gt.megapaca.mproject.domain.repository.CompanyRepository
import gt.megapaca.mproject.domain.repository.CountryRepository
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach

open class CompanyModuleUnitTestCase {

    protected lateinit var companyRepository: CompanyRepository
    protected lateinit var countryRepository: CountryRepository
    protected lateinit var companyCreator: CompanyCreator
    protected lateinit var companyUpdater: CompanyUpdater
    protected lateinit var companyFinder: CompanyFinder

    @BeforeEach
    protected fun setUp() {
        companyRepository = mockk()
        countryRepository = mockk()
        companyCreator = CompanyCreator(companyRepository, countryRepository)
        companyUpdater = CompanyUpdater(companyRepository, countryRepository)
        companyFinder = CompanyFinder(companyRepository)
    }
}