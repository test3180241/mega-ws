package gt.megapaca.mproject.application.company.shared

import gt.megapaca.mproject.application.company.CompanyRequest
import gt.megapaca.mproject.company.CompanyCountryMother
import gt.megapaca.mproject.company.CompanyNameMother
import gt.megapaca.mproject.company.CompanyTaxCodeMother
import gt.megapaca.mproject.domain.entities.company.Company
import gt.megapaca.mproject.domain.entities.company.CompanyCountry
import gt.megapaca.mproject.domain.entities.company.CompanyName
import gt.megapaca.mproject.domain.entities.company.CompanyTaxCode
import gt.megapaca.mproject.domain.shared.LongMother

object CompanyMother {

    fun create(
        name: CompanyName,
        country: CompanyCountry,
        taxCode: CompanyTaxCode
    ): Company {
        return Company(
            name,
            country,
            taxCode
        )
    }

    fun create(
        id: Long,
        name: CompanyName,
        country: CompanyCountry,
        taxCode: CompanyTaxCode
    ): Company {
        val company = Company(
            name,
            country,
            taxCode
        )
        company.id = id
        return company
    }

    fun fromRequest(request: CompanyRequest): Company {
        val company = create(
            CompanyName(request.name),
            CompanyCountry(request.country),
            CompanyTaxCode(request.taxCode)
        )
        company.id = request.id
        company.active = request.active
        return company
    }

    fun random(): Company {
        return Company(
            CompanyNameMother.random(),
            CompanyCountryMother.random(),
            CompanyTaxCodeMother.random()
        ).apply {
            this.id = LongMother.randomNotZero()
        }
    }

    fun randomRequest(): CompanyRequest {
        val random = random()
        return CompanyRequest.build {
            this.id = random.id
            this.name = random.name.value
            this.country = random.country.value
            this.taxCode = random.taxCode.value
        }
    }

    fun randomList(size: Int): List<Company> {
        val companys = arrayListOf<Company>()
        for (i in 0..size) {
            companys.add(CompanyMother.random())
        }
        return companys
    }
}