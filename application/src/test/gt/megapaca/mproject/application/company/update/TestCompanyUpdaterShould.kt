package company.update

import gt.megapaca.mproject.application.company.shared.CompanyModuleUnitTestCase
import gt.megapaca.mproject.application.company.shared.CompanyMother
import gt.megapaca.mproject.application.country.shared.CountryMother
import gt.megapaca.mproject.application.exceptions.EntityNotFoundException
import gt.megapaca.mproject.application.exceptions.InvalidTaxCodeException
import io.mockk.every
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestCompanyUpdaterShould : CompanyModuleUnitTestCase() {

    @Test
    fun `throw exception when company does not exists`() {
        val request = CompanyMother.randomRequest()

        //1. NO PASA
        every {
            companyRepository.findById(allAny())
        } returns Optional.empty()

        assertThrows(
            EntityNotFoundException::class.java,
            { companyUpdater.update(request) },
            "throw exception because company does not exists"
        )
    }

    @Test
    fun `throw exception when country does not exists`() {
        val request = CompanyMother.randomRequest()
        val company = CompanyMother.fromRequest(request)
        //1. SI PASA
        every {
            companyRepository.findById(allAny())
        } returns Optional.of(company)

        //2.NO PASA
        every {
            countryRepository.findById(allAny())
        } returns Optional.empty()

        assertThrows(
            EntityNotFoundException::class.java,
            { companyUpdater.update(request) },
            "throw exception because country does not exists"
        )

    }

    @Test
    fun `throw exception when country tax code format does not match`() {
        val request = CompanyMother.randomRequest()
        val company = CompanyMother.fromRequest(request)
        val country = CountryMother.random()

        //1. SI PASA
        every {
            companyRepository.findById(allAny())
        } returns Optional.of(company)
        //2. SI PASA
        every {
            countryRepository.findById(allAny())
        } returns Optional.of(country)

        //3. NO HACE MATCH
        country.taxCodeFormat.value = "[ABC]"
        request.taxCode = "123"

        assertThrows(
            InvalidTaxCodeException::class.java,
            { companyUpdater.update(request) },
            "throw exception because country does not match"
        )

    }

    @Test
    fun `update a company`() {
        val request = CompanyMother.randomRequest()
        val company = CompanyMother.fromRequest(request)
        val country = CountryMother.random()

        //1. SI PASA
        every {
            companyRepository.findById(allAny())
        } returns Optional.of(company)
        //2. SI PASA
        every {
            countryRepository.findById(allAny())
        } returns Optional.of(country)

        //3. HACE MATCH
        country.taxCodeFormat.value = "[1-4]*"
        request.taxCode = "12"
        //4. ACTUALIZA EMPRESA
        every {
            companyRepository.update(allAny())
        } returns request.id

        assertEquals(
            companyUpdater.update(request),
            request.id,
            "Country ID must be equals"
        )

    }


}