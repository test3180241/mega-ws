package gt.megapaca.mproject.application.country.find

import gt.megapaca.mproject.application.country.shared.CountryModuleUnitTestCase
import gt.megapaca.mproject.application.country.shared.CountryMother
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import io.mockk.every
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.*

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestCountryByIdFinderShould : CountryModuleUnitTestCase(){

    @Test
    fun `return empty when country not exists`() {
        val country = CountryMother.random()
        country.id = 0

        every {
            countryRepository.findById(allAny())
        } returns Optional.empty()

        val result = countryByIdFinder.find(country.id)
        assertTrue(result.isEmpty, "Response must be empty")
    }

    @Test
    fun `find an existing country`() {
        val countryBase = CountryMother.random()
        val countryResponse = CountryResponse(countryBase)

        every {
            countryRepository.findById(allAny())
        } returns Optional.of(countryBase)

        val finder = countryByIdFinder.find(countryBase.id)
        assertTrue(finder.isPresent, "Country must be present")

        val country = finder.get()
        assertEquals(
            countryResponse,
            country,
            "Country must be equals"
        )
        assertEquals(
            countryResponse.id,
            country.id,
            "Country id must be equals"
        )
        assertEquals(
            countryResponse.name,
            country.name,
            "Country name must be equals"
        )
        assertEquals(
            countryResponse.taxCodeFormat,
            country.taxCodeFormat,
            "Country taxCodeFormat must be equals"
        )

    }
}