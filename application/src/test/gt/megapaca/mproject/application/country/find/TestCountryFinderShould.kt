package gt.megapaca.mproject.application.country.find

import gt.megapaca.mproject.application.country.shared.CountryModuleUnitTestCase
import gt.megapaca.mproject.application.country.shared.CountryMother
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import io.mockk.every
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.util.stream.Collectors

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestCountryFinderShould : CountryModuleUnitTestCase() {

    @Test
    fun `return empty list when country not found`() {
        every {
            countryRepository.findAll()
        } returns emptyList()

        val result = countryFinder.findAll()

        assertTrue(result.isEmpty(), "List must be empty")
    }

    @Test
    fun `return country list data`() {
        val listBase = CountryMother.randomList(3)

        val responseListBase = listBase
            .stream()
            .map { CountryResponse(it) }
            .collect(Collectors.toList())

        every {
            countryRepository.findAll()
        } returns listBase

        val result = countryFinder.findAll()

        assertEquals(responseListBase, result, "Country lists must be equals")
    }

}