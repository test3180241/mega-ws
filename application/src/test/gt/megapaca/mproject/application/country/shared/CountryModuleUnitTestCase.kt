package gt.megapaca.mproject.application.country.shared

import gt.megapaca.mproject.application.country.find.CountryByIdFinder
import gt.megapaca.mproject.application.country.find.CountryFinder
import gt.megapaca.mproject.domain.repository.CountryRepository
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach

open class CountryModuleUnitTestCase {

    protected lateinit var countryRepository: CountryRepository
    protected lateinit var countryFinder: CountryFinder
    protected lateinit var countryByIdFinder: CountryByIdFinder

    @BeforeEach
    protected fun setUp() {
        countryRepository = mockk()
        countryFinder = CountryFinder(countryRepository)
        countryByIdFinder = CountryByIdFinder(countryRepository)
    }
}