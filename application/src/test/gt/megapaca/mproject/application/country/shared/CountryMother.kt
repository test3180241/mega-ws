package gt.megapaca.mproject.application.country.shared

import gt.megapaca.mproject.application.country.CountryRequest
import gt.megapaca.mproject.country.CountryNameMother
import gt.megapaca.mproject.country.CountryTaxCodeFormatMother
import gt.megapaca.mproject.domain.entities.company.Company
import gt.megapaca.mproject.domain.entities.company.CompanyCountry
import gt.megapaca.mproject.domain.entities.company.CompanyName
import gt.megapaca.mproject.domain.entities.company.CompanyTaxCode
import gt.megapaca.mproject.domain.entities.country.Country
import gt.megapaca.mproject.domain.entities.country.CountryName
import gt.megapaca.mproject.domain.entities.country.CountryTaxCodeFormat
import gt.megapaca.mproject.domain.shared.LongMother

object CountryMother {

    fun create(
        name: CountryName,
        taxCodeFormat: CountryTaxCodeFormat
    ): Country {
        return Country(
            name,
            taxCodeFormat
        )
    }

    fun create(
        id: Long,
        name: CountryName,
        taxCodeFormat: CountryTaxCodeFormat
    ): Country {
        val country = Country(
            name,
            taxCodeFormat
        )
        country.id = id
        return country
    }

    fun random(): Country {
        return Country(
            CountryNameMother.random(),
            CountryTaxCodeFormatMother.random()
        ).apply {
            this.id = LongMother.randomNotZero()
        }
    }

    fun randomRequest(): CountryRequest {
        val random = random()
        return CountryRequest.build {
            this.id = random.id
            this.name = random.name.value
            this.taxCodeFormat = random.taxCodeFormat.value
        }
    }

    fun randomList(size: Int): List<Country> {
        val countrys = arrayListOf<Country>()
        for (i in 0..size) {
            countrys.add(random())
        }
        return countrys
    }
}