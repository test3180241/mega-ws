package gt.megapaca.mproject.domain.entities.company

data class Company(
    var name: CompanyName,
    var country: CompanyCountry,
    var taxCode: CompanyTaxCode
) {

    var id = 0L
    var active: Boolean = true
}