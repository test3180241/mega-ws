package gt.megapaca.mproject.domain.entities.company

import gt.megapaca.mproject.domain.exceptions.RequiredValueException


data class CompanyCountry(var value: Long) {

    init{
        ensureHasCountry(value)
    }

    private fun ensureHasCountry(value: Long){
        if (value <= 0)
            throw RequiredValueException("Country is required")
    }

}