package gt.megapaca.mproject.domain.entities.company

import gt.megapaca.mproject.domain.exceptions.RequiredValueException

data class CompanyTaxCode(var value: String) {

    init {
        ensureHasTaxCode(value)
    }

    private fun ensureHasTaxCode(value: String) {
        if ("" == value) {
            throw RequiredValueException("Tax code is required")
        }
    }
}