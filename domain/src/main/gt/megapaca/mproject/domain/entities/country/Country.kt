package gt.megapaca.mproject.domain.entities.country

data class Country(
    var name: CountryName,
    var taxCodeFormat: CountryTaxCodeFormat
) {
    var id = 0L
    var active: Boolean = true
}