package gt.megapaca.mproject.domain.entities.country

import gt.megapaca.mproject.domain.exceptions.RequiredValueException

data class CountryName(var value: String) {

    init {
        ensureHasName(value)
    }

    private fun ensureHasName(value: String) {
        if ("" == value) {
            throw RequiredValueException("Name is required")
        }
    }
}