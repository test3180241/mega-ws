package gt.megapaca.mproject.domain.entities.country

import gt.megapaca.mproject.domain.exceptions.RequiredValueException

data class CountryTaxCodeFormat(var value: String) {

    init {
        ensureHasTaxCodeFormat(value)
    }

    private fun ensureHasTaxCodeFormat(value: String) {
        if ("" == value) {
            throw RequiredValueException("Tax code format is required")
        }
    }
}