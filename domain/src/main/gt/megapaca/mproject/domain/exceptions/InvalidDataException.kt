package gt.megapaca.mproject.domain.exceptions

class InvalidDataException(message: String) : Exception(message)
