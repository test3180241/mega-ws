package gt.megapaca.mproject.domain.exceptions

class RequiredValueException(errorMessage: String) : Exception("Error en entradas de usuario. $errorMessage")
