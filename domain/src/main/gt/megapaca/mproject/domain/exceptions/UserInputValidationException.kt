package gt.megapaca.mproject.domain.exceptions

data class UserInputValidationException(val errors: List<String>) : Exception("Error en entradas de usuario") {
    override fun toString(): String {
        return "{message=$message}, errors=$errors"
    }
}
