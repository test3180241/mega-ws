package gt.megapaca.mproject.domain.repository

import gt.megapaca.mproject.domain.entities.company.Company
import java.util.*

interface CompanyRepository {

    fun create(company: Company): Long

    fun update(company: Company): Long

    fun findById(id: Long): Optional<Company>

    fun findAll(): List<Company>

}