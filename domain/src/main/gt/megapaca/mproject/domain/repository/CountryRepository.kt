package gt.megapaca.mproject.domain.repository

import gt.megapaca.mproject.domain.entities.country.Country
import java.util.*

interface CountryRepository {

    fun findById(id: Long): Optional<Country>

    fun findAll(): List<Country>
}