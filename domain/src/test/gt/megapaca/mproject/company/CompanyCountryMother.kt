package gt.megapaca.mproject.company

import gt.megapaca.mproject.domain.entities.company.CompanyCountry
import gt.megapaca.mproject.domain.shared.LongMother

object CompanyCountryMother {

    fun create(value: Long): CompanyCountry {
        return CompanyCountry(value)
    }

    fun random(): CompanyCountry {
        return create(LongMother.randomNotZero())
    }
}