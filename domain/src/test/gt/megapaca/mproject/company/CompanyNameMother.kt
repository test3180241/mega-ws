package gt.megapaca.mproject.company

import gt.megapaca.mproject.domain.entities.company.CompanyName
import gt.megapaca.mproject.domain.shared.WordMother

object CompanyNameMother {

    fun create(value: String): CompanyName {
        return CompanyName(value)
    }

    fun random(): CompanyName {
        return create(WordMother.random())
    }
}