package gt.megapaca.mproject.company

import gt.megapaca.mproject.domain.entities.company.CompanyTaxCode
import gt.megapaca.mproject.domain.shared.WordMother

object CompanyTaxCodeMother {

    fun create(value: String): CompanyTaxCode {
        return CompanyTaxCode(value)
    }

    fun random(): CompanyTaxCode {
        return create(WordMother.random())
    }
}