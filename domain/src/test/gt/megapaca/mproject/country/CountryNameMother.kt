package gt.megapaca.mproject.country

import gt.megapaca.mproject.domain.entities.country.CountryName
import gt.megapaca.mproject.domain.shared.WordMother

object CountryNameMother {

    fun create(value: String): CountryName {
        return CountryName(value)
    }

    fun random(): CountryName {
        return create(WordMother.random())
    }
}