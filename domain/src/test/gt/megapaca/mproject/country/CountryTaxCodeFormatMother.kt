package gt.megapaca.mproject.country

import gt.megapaca.mproject.domain.entities.country.CountryTaxCodeFormat
import gt.megapaca.mproject.domain.shared.WordMother

object CountryTaxCodeFormatMother {

    fun create(value: String): CountryTaxCodeFormat {
        return CountryTaxCodeFormat(value)
    }

    fun random(): CountryTaxCodeFormat {
        return create(WordMother.random())
    }
}