package gt.megapaca.mproject.domain.shared

import java.math.BigDecimal

object BigDecimalMother {

    fun random(): BigDecimal {
        return BigDecimal.valueOf(MotherCreator.random().number().randomNumber())
    }

    fun randomNotZero(): BigDecimal {
        return BigDecimal.valueOf(MotherCreator.random().number().numberBetween(1L, 1000L))
    }

}
