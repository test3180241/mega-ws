package gt.megapaca.mproject.domain.shared

object BooleanMother {

    fun random(): Boolean {
        return MotherCreator.random().bool().bool()
    }

}