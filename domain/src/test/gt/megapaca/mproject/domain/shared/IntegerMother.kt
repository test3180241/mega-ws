package gt.megapaca.mproject.domain.shared

object IntegerMother {

    fun random(): Int {
        return MotherCreator.random().number().randomDigit()
    }

    fun randomNotZero(): Int {
        return MotherCreator.random().number().numberBetween(1, 1000)
    }

    fun randomList(size: Int): List<Int> {
        val intList = arrayListOf<Int>()
        for (i in 0..size) {
            intList.add(randomNotZero())
        }
        return intList
    }

}
