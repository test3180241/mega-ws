package gt.megapaca.mproject.domain.shared

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable

object ListPageMother {

    fun <T> randomPage(
        size: Int,
        pageable: Pageable = PageRequest.of(0, 20),
        model: () -> T
    ): Page<T> {
        val list = randomList(size, model)
        return PageImpl(list, pageable, size.toLong())
    }

    fun <T> randomList(size: Int, model: () -> T): MutableList<T> {
        val list: MutableList<T> = ArrayList()
        for (i in 1..size) {
            list.add(model())
        }
        return list
    }

}
