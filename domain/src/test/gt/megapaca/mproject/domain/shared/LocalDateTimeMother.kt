package gt.megapaca.mproject.domain.shared

import gt.megapaca.mproject.domain.shared.MotherCreator.random
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*
import java.util.concurrent.TimeUnit

object LocalDateTimeMother {

    private val now: Date = Date()

    fun randomFuture(): LocalDateTime {
        val random: Date = random().date().future(1, TimeUnit.DAYS, now)
        return Instant.ofEpochMilli(random.time)
            .atZone(ZoneId.systemDefault())
            .toLocalDateTime()
    }

    fun randomPast(): LocalDateTime {
        val random: Date = random().date().past(1, TimeUnit.DAYS, now)
        return Instant.ofEpochMilli(random.time)
            .atZone(ZoneId.systemDefault())
            .toLocalDateTime()
    }

}
