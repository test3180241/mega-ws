package gt.megapaca.mproject.domain.shared

object LongMother {

    fun randomNotZero(): Long {
        return MotherCreator.random().number().numberBetween(1L, 1000L)
    }

}