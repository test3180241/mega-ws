package gt.megapaca.mproject.domain.shared

import com.github.javafaker.Faker

object MotherCreator {
    private val faker = Faker()

    fun random(): Faker {
        return faker
    }
}
