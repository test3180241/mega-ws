package gt.megapaca.mproject.domain.shared

object WordMother {

    fun random(): String {
        return MotherCreator.random().lorem().word()
    }

    @JvmStatic
    fun randomNotEmpty(): String {
        val value = MotherCreator.random().lorem().word()
        return when {
            value.isNotEmpty() -> value
            else -> "Random"
        }
    }

    fun randomList(size: Int): List<String> {
        val stringList = arrayListOf<String>()

        for (i in 0..size) {
            val str = random()
            stringList.add(str)
        }

        return stringList
    }

}
