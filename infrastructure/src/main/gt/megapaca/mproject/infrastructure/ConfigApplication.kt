package gt.megapaca.mproject.infrastructure

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@ComponentScan(basePackages = ["gt.megapaca.*.infrastructure.config"])
@EntityScan(basePackages = ["gt.megapaca.mproject.infrastructure.hibernate.entities"])
@EnableJpaRepositories(basePackages = ["gt.megapaca.mproject.infrastructure.hibernate.repo"])
@EnableAutoConfiguration
class ConfigApplication
