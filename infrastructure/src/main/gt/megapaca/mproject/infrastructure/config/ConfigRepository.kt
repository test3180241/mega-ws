package gt.megapaca.mproject.infrastructure.config

import gt.megapaca.mproject.domain.repository.CompanyRepository
import gt.megapaca.mproject.domain.repository.CountryRepository
import gt.megapaca.mproject.infrastructure.hibernate.repo.CompanyJpaRepository
import gt.megapaca.mproject.infrastructure.hibernate.repo.CountryJpaRepository
import gt.megapaca.mproject.infrastructure.repository.PostgresCompanyRepository
import gt.megapaca.mproject.infrastructure.repository.PostgresCountryRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ConfigRepository {

    @Bean
    fun companyRepository(
        companyJpaRepository: CompanyJpaRepository
    ): CompanyRepository {
        return PostgresCompanyRepository(companyJpaRepository)
    }

    @Bean
    fun countryRepository(
        countryJpaRepository: CountryJpaRepository
    ): CountryRepository {
        return PostgresCountryRepository(countryJpaRepository)
    }

}