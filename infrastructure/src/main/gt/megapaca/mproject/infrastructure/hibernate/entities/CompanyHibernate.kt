package gt.megapaca.mproject.infrastructure.hibernate.entities

import org.hibernate.envers.Audited
import javax.persistence.*

@Entity(name = "Company")
@Table(name = "company", schema = "mega")
class CompanyHibernate {

    @Id //Identificador de la entidad
    @Column(updatable = false) //La propiedad no se puede actualizar
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Se va a generar automaticamente el id
    var id: Long = 0

    var active: Boolean = true

    var name: String = ""

    var taxCode: String = ""

    var country: Long = 0


}