package gt.megapaca.mproject.infrastructure.hibernate.entities

import javax.persistence.*

@Entity(name = "Country")
@Table(name = "country", schema = "mega")
class CountryHibernate {

    @Id
    @Column(updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    var active: Boolean = true

    var name: String = ""

    var taxCodeFormat: String = ""
}