package gt.megapaca.mproject.infrastructure.hibernate.repo

import gt.megapaca.mproject.infrastructure.hibernate.entities.CompanyHibernate
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface CompanyJpaRepository : JpaRepository<CompanyHibernate, Long> {

    @Query(
        value = "FROM Company a " +
                " WHERE a.active = true " +
                " ORDER BY a.id DESC "
    )
    fun findAllActive(): List<CompanyHibernate>
}