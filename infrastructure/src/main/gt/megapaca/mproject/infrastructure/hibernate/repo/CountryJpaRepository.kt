package gt.megapaca.mproject.infrastructure.hibernate.repo

import gt.megapaca.mproject.infrastructure.hibernate.entities.CountryHibernate
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface CountryJpaRepository : JpaRepository<CountryHibernate, Long>{

    @Query(
        value = "FROM Country a " +
                " WHERE a.active = true " +
                " ORDER BY a.id DESC "
    )
    fun findAllActive(): List<CountryHibernate>
}