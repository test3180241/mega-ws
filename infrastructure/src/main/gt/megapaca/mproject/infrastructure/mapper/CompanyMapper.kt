package gt.megapaca.mproject.infrastructure.mapper

import gt.megapaca.mproject.domain.entities.company.Company
import gt.megapaca.mproject.domain.entities.company.CompanyCountry
import gt.megapaca.mproject.domain.entities.company.CompanyName
import gt.megapaca.mproject.domain.entities.company.CompanyTaxCode
import gt.megapaca.mproject.infrastructure.hibernate.entities.CompanyHibernate


class CompanyMapper {

    fun fromHibernateToDomain(hibernate: CompanyHibernate): Company {
        return Company(
            CompanyName(hibernate.name),
            CompanyCountry(hibernate.country),
            CompanyTaxCode(hibernate.taxCode)
        ).apply {
            this.id = hibernate.id
            this.active = hibernate.active
        }
    }

    fun fromDomainToHibernate(domain: Company): CompanyHibernate {
        return CompanyHibernate().apply {
            this.name = domain.name.value
            this.country = domain.country.value
            this.taxCode = domain.taxCode.value
        }
    }
}