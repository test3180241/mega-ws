package gt.megapaca.mproject.infrastructure.mapper

import gt.megapaca.mproject.domain.entities.country.Country
import gt.megapaca.mproject.domain.entities.country.CountryName
import gt.megapaca.mproject.domain.entities.country.CountryTaxCodeFormat
import gt.megapaca.mproject.infrastructure.hibernate.entities.CountryHibernate


class CountryMapper {

    fun fromHibernateToDomain(hibernate: CountryHibernate): Country {
        return Country(
            CountryName(hibernate.name),
            CountryTaxCodeFormat(hibernate.taxCodeFormat)
        ).apply {
            this.id = hibernate.id
            this.active = hibernate.active
        }
    }

    fun fromDomainToHibernate(domain: Country): CountryHibernate {
        return CountryHibernate().apply {
            this.name = domain.name.value
            this.taxCodeFormat = domain.taxCodeFormat.value
        }
    }
}