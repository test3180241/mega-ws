package gt.megapaca.mproject.infrastructure.repository

import gt.megapaca.mproject.application.exceptions.EntityNotFoundException
import gt.megapaca.mproject.domain.entities.company.Company
import gt.megapaca.mproject.domain.repository.CompanyRepository
import gt.megapaca.mproject.infrastructure.hibernate.repo.CompanyJpaRepository
import gt.megapaca.mproject.infrastructure.mapper.CompanyMapper
import java.util.*

class PostgresCompanyRepository(
    private val companyJpaRepository: CompanyJpaRepository
) : CompanyRepository {

    private val companyMapper = CompanyMapper()

    override fun create(company: Company): Long {
        val entity = companyMapper.fromDomainToHibernate(company)
        return companyJpaRepository.save(entity).id
    }

    override fun update(company: Company): Long {
        val entity = companyJpaRepository.findById(company.id)
            .orElseThrow { EntityNotFoundException("Entity company not found") }

        entity.active = company.active
        entity.name = company.name.value
        entity.country = company.country.value
        entity.taxCode = company.taxCode.value

        return companyJpaRepository.save(entity).id
    }

    override fun findById(id: Long): Optional<Company> {
        val entity = companyJpaRepository.findById(id)

        return when {
            entity.isPresent -> Optional.of(companyMapper.fromHibernateToDomain(entity.get()))
            else -> Optional.empty()
        }
    }

    override fun findAll(): List<Company> {
        val dataList = companyJpaRepository.findAllActive()
        return dataList.map { companyMapper.fromHibernateToDomain(it) }
    }

}
