package gt.megapaca.mproject.infrastructure.repository

import gt.megapaca.mproject.domain.entities.country.Country
import gt.megapaca.mproject.domain.repository.CountryRepository
import gt.megapaca.mproject.infrastructure.hibernate.repo.CountryJpaRepository
import gt.megapaca.mproject.infrastructure.mapper.CountryMapper
import java.util.*

class PostgresCountryRepository(
    private val countryJpaRepository: CountryJpaRepository
) : CountryRepository {

    private val countryMapper = CountryMapper()

    override fun findById(id: Long): Optional<Country> {
        val entity = countryJpaRepository.findById(id)

        return when {
            entity.isPresent -> Optional.of(countryMapper.fromHibernateToDomain(entity.get()))
            else -> Optional.empty()
        }
    }

    override fun findAll(): List<Country> {
        val dataList = countryJpaRepository.findAllActive()
        return dataList.map { countryMapper.fromHibernateToDomain(it) }
    }
}