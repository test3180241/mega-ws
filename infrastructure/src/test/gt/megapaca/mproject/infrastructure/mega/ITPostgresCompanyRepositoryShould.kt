package gt.megapaca.mproject.infrastructure.mega

import gt.megapaca.mproject.application.company.shared.CompanyMother
import gt.megapaca.mproject.infrastructure.mega.shared.CompanyInfrastructureTestCase
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertEquals

class ITPostgresCompanyRepositoryShould : CompanyInfrastructureTestCase() {

    @Test
    fun `create a company`() {
        val company = CompanyMother.random()
        val companyCreated = repository.create(company)

        assertTrue(companyCreated > 0, "ID must be greater than 0")
    }

    @Test
    fun `create a company not null`() {
        val company = CompanyMother.random()
        val companyCreated = repository.create(company)

        assertNotNull(companyCreated, "ID must not be null")
    }

    @Test
    fun `update an existing company`() {
        val company = CompanyMother.random()
        val companyCreated = repository.create(company)

        val companyToUpdate = CompanyMother.create(
            companyCreated,
            company.name,
            company.country,
            company.taxCode
        )

        repository.update(companyToUpdate)

        val companyAlreadyUpdated = repository.findById(companyCreated)
        assertEquals(
            companyAlreadyUpdated.orElse(null),
            companyToUpdate, "The company must be equals"
        )
    }

    @Test
    fun `find all`() {
        val company = CompanyMother.random()
        repository.create(company)

        val list = repository.findAll()
        assertTrue(list.isNotEmpty(), "Company list is not empty")
    }
}