package gt.megapaca.mproject.infrastructure.mega

import gt.megapaca.mproject.application.country.shared.CountryMother
import gt.megapaca.mproject.domain.entities.country.Country
import gt.megapaca.mproject.domain.entities.country.CountryName
import gt.megapaca.mproject.domain.entities.country.CountryTaxCodeFormat
import gt.megapaca.mproject.infrastructure.mega.shared.CountryInfrastructureTestCase
import org.jetbrains.annotations.NotNull
import org.junit.Test
import org.springframework.test.context.jdbc.Sql
import org.junit.jupiter.api.Assertions.assertTrue

@Sql(scripts = ["classpath:country_data.sql"], executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class ITPostgresCountryRepositoryShould : CountryInfrastructureTestCase() {


    @Test
    fun `get an existing country by id`() {

        val exist = repository.findById(-99)
        assertTrue(exist.isPresent, "The country ID must be greater than 0")
    }

    @Test
    fun `return empty when country by id not exists`() {

        val exist = repository.findById(-98)
        assertTrue(exist.isEmpty, "Response must be empty")
    }

    @Test
    fun `find all`() {

        val list = repository.findAll()
        assertTrue(list.isNotEmpty(), "Country list is not empty")
    }

}