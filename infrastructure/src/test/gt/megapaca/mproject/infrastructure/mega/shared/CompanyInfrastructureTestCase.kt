package gt.megapaca.mproject.infrastructure.mega.shared

import gt.megapaca.mproject.domain.repository.CompanyRepository
import gt.megapaca.mproject.infrastructure.shared.ContextInfrastructureITCase
import org.springframework.beans.factory.annotation.Autowired

abstract class CompanyInfrastructureTestCase : ContextInfrastructureITCase() {

    @Autowired
    lateinit var repository: CompanyRepository

}
