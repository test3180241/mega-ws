package gt.megapaca.mproject.infrastructure.mega.shared


import gt.megapaca.mproject.domain.repository.CountryRepository
import gt.megapaca.mproject.infrastructure.shared.ContextInfrastructureITCase
import org.springframework.beans.factory.annotation.Autowired

abstract class CountryInfrastructureTestCase : ContextInfrastructureITCase() {

    @Autowired
    lateinit var repository: CountryRepository
}